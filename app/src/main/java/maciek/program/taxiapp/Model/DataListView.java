package maciek.program.taxiapp.Model;

/**
 * Created by root on 13.02.17.
 */

public class DataListView {

    private String tytul;
    private String opis;

    public DataListView(String tytul,String opis) {
        this.opis = opis;
        this.tytul = tytul;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }
}
