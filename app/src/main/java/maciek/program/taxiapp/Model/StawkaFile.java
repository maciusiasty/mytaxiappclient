package maciek.program.taxiapp.Model;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Iterator;
import java.util.StringTokenizer;

/**
 * Created by maciek on 03.03.17.
 */




public class StawkaFile {

    /*
    @Description zapisuje stawke w PLN do Json'a
     */

    public boolean dodajStawka(Stawka stawka, Context context) {

        // Get the directory for the user's public pictures directory.
        final File path =
                Environment.getExternalStoragePublicDirectory
                        (
                                //Environment.DIRECTORY_PICTURES
                                Environment.DIRECTORY_DCIM + "/"
                        );
        FileOutputStream fOut = null;

        // Make sure the path directory exists.
        if (!path.exists()) {
            // Make it, if it doesn't exit
            path.mkdirs();
        }

        final File file = new File(path, "stawka.txt");

        // Save your stream, don't forget to flush() it before closing it.

        try {
            file.createNewFile();
             fOut = new FileOutputStream(file);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(String.valueOf(stawka.getStawka()));

            myOutWriter.close();

            fOut.flush();
            fOut.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        if(fOut == null)
            return false;
        else return true;

    }


    public Stawka czytajStawka(Context context) {

        String wynik = null;
        final File path =
                Environment.getExternalStoragePublicDirectory
                        (
                                //Environment.DIRECTORY_PICTURES
                                Environment.DIRECTORY_DCIM + "/"
                        );
        // Make sure the path directory exists.
        if (!path.exists()) {
            // Make it, if it doesn't exit
            path.mkdirs();
        }
        FileInputStream inputStream = null;

        final File file = new File(path, "stawka.txt");


        try {
             inputStream = new FileInputStream(file);


            byte fileContent[] = new byte[(int)file.length()];

            inputStream.read(fileContent);
            wynik = new String(fileContent);

                inputStream.close();


        }
        catch (FileNotFoundException e) {
            System.out.println("File not found" + e);
        }catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        finally {
                try {
                    if (inputStream != null)
                        inputStream.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

        }

        if(wynik != null) {
            Stawka stawka = new Stawka();
            stawka.setStawka(Double.valueOf(wynik.toString()));
            return stawka;
        } else return null;


    }
}
