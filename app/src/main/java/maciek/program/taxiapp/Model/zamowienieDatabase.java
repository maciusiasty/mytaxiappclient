package maciek.program.taxiapp.Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 02.02.17.
 */

public class zamowienieDatabase extends SQLiteOpenHelper {

    /*
    _______________________________________________________

                    ZAMÓWIENIE DATABASE
      ID_ZAMOWIENIA INTEGER PRIMARY KEY NOT NULL
      ID_Klienta    INTEGER PRIMARY KEY NOT NULL
      ID_Kierowca   INTEGER PRIMARY KEY NOT NULL
      Ocena         INTEGER
      Kwota         DOUBLE
     */
    private static final String NAZWABAZY_ZAMOWIENIE = "zamowienie";
    private static final Integer WERSJA_BAZY = 1;
    private static final String ID_ZAMOWIENIA = "id_zamowienia";
    private static final String ID_KLIENTA = "id_klienta";
    private static final String ID_KIEROWCA = "id_kierowca";
    private static final String OCENA = "ocena";
    private static final String KWOTA = "kwota";


    public zamowienieDatabase(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String ZamowienieSQL = "CREATE TABLE "+NAZWABAZY_ZAMOWIENIE+"("+ID_ZAMOWIENIA+" INTEGER PRIMARY KEY NOT NULL,"+
                ID_KLIENTA+" INTEGER NOT NULL,"+
                ID_KIEROWCA+" INTEGER NOT NULL,"+
                OCENA+" INTEGER,"+
                KWOTA+" DOUBLE,"+
                "FOREIGN KEY ("+ID_KLIENTA+") REFERENCES klient(id),"+
                "FOREIGN KEY ("+ID_KIEROWCA+") REFERENCES kierowca(id);";
        db.execSQL(ZamowienieSQL);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS "+NAZWABAZY_ZAMOWIENIE);
        onCreate(db);

    }

    public boolean addZamowienie(Zamowienie zamowienie) {

        int czyUdalosie = -1;
        boolean czyudaloSie = false;

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        try {
            ContentValues contentValues = new ContentValues();

            contentValues.put(ID_KIEROWCA, zamowienie.getKierowca().getId());
            contentValues.put(ID_KLIENTA, zamowienie.getKlient().getId());
            contentValues.put(OCENA, zamowienie.getOcena());
            contentValues.put(KWOTA, zamowienie.getKwota());

            czyUdalosie = (int) sqLiteDatabase.insertOrThrow(NAZWABAZY_ZAMOWIENIE, null, contentValues);
            sqLiteDatabase.setTransactionSuccessful();
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            sqLiteDatabase.endTransaction();
            if(czyUdalosie > -1)
                czyudaloSie = true;
            else czyudaloSie = false;

        }
        return czyudaloSie;

    }


    public List<Zamowienie> getListOfZamowienie() {

        String MY_ZAMOWIENIE_QUERY = String.format("SELECT * FROM %s ",
                NAZWABAZY_ZAMOWIENIE);

        SQLiteDatabase db = getWritableDatabase();

        Cursor cursor = db.rawQuery(MY_ZAMOWIENIE_QUERY,null);

        ArrayList<Zamowienie> arrayOfZamowienie = new ArrayList<Zamowienie>();

        while(cursor.moveToNext()) {

            Zamowienie zamowienie = new Zamowienie();


        }
        return arrayOfZamowienie;
    }
}

