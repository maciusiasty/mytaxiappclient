package maciek.program.taxiapp.Model;

/**
 * Created by root on 02.02.17.
 */

public class Zamowienie {

    private Integer id;
    private Klient klient;
    private Kierowca kierowca;
    private String ocena;
    private Double kwota;

    public Kierowca getKierowca() {
        return kierowca;
    }

    public void setKierowca(Kierowca kierowca) {
        this.kierowca = kierowca;
    }

    public Klient getKlient() {
        return klient;
    }

    public void setKlient(Klient klient) {
        this.klient = klient;
    }

    public Double getKwota() {
        return kwota;
    }

    public void setKwota(Double kwota) {
        this.kwota = kwota;
    }

    public String getOcena() {
        return ocena;
    }

    public void setOcena(String ocena) {
        this.ocena = ocena;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
