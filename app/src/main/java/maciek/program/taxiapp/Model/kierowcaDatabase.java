package maciek.program.taxiapp.Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by root on 23.01.17.
 */

public class kierowcaDatabase extends SQLiteOpenHelper {

    /*
            TABELA KIEROWCA
    ____________________________________________________________
                id INTEGER PRIMARY KEY
                FOREIGN_KEY(id_klient) REFERENCES klient(id)
               imieINazwisko TEXT NOT NULL
               ocena INTEGER NOT NULL
     ____________________________________________________________
     */
    private static final String NAZWA_BAZY_KIEROWCA = "kierowca2";
    private static final String ID_KIEROWCA = "id";
    private static final String IMIE_KIEROWCA = "imie";
    private static final String NAZWISKO_KIEROWCA = "nazwisko";
    private static final String LOGIN_KIEROWCA = "login";
    private static final String HASLO_KIEROWCA = "haslo";
    private static final int WERSJA_BAZY_KIEROWCA = 1;





    public static kierowcaDatabase instance = null;



  public kierowcaDatabase(Context context) {
        super(context, NAZWA_BAZY_KIEROWCA, null, WERSJA_BAZY_KIEROWCA);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        String kierowcaSQL = "CREATE TABLE "+NAZWA_BAZY_KIEROWCA+"("+ID_KIEROWCA+" INTEGER PRIMARY KEY,"+
                IMIE_KIEROWCA+" TEXT NOT NULL,"+NAZWISKO_KIEROWCA+" TEXT NOT NULL,"+
                LOGIN_KIEROWCA + " TEXT NOT NULL,"+
                HASLO_KIEROWCA + " TEXT NOT NULL);";




        db.execSQL(kierowcaSQL);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(oldVersion !=newVersion) {

            db.execSQL("DROP TABLE IF EXISTS " + NAZWA_BAZY_KIEROWCA);

            onCreate(db);
        }

    }


    public boolean addKierowca(Kierowca kierowca) {
        int czyUdalosie=-1;

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        sqLiteDatabase.beginTransaction();
        Kierowca kierowcaSzukaj = checkIfSimilar(kierowca);
        if(kierowcaSzukaj!=null)
            return false;



        try {
            ContentValues contentValues = new ContentValues();

            contentValues.put(IMIE_KIEROWCA, kierowca.getImie());
            contentValues.put(NAZWISKO_KIEROWCA,kierowca.getNazwisko());
            contentValues.put(HASLO_KIEROWCA,kierowca.getHaslo());
            contentValues.put(LOGIN_KIEROWCA,kierowca.getLogin());

            czyUdalosie = (int) sqLiteDatabase.insertOrThrow(NAZWA_BAZY_KIEROWCA, null, contentValues);
            sqLiteDatabase.setTransactionSuccessful();
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        finally {
            sqLiteDatabase.endTransaction();

            if(czyUdalosie >-1) return true;
            else return false;
        }

    }



    public List<Kierowca> getListOfKierowca() {

        String MY_KIEROWCA_QUERY = String.format("SELECT * FROM %s ",
                NAZWA_BAZY_KIEROWCA);

        SQLiteDatabase db = getWritableDatabase();

        Cursor cursor = db.rawQuery(MY_KIEROWCA_QUERY,null);

        ArrayList<Kierowca> arrayOfKierowca = new ArrayList<Kierowca>();

        while(cursor.moveToNext()) {

            Kierowca kierowca = new Kierowca();
            kierowca.setId(cursor.getInt(cursor.getColumnIndex("id")));
            kierowca.setImie(cursor.getString(cursor.getColumnIndex("imie")));
            kierowca.setNazwisko(cursor.getString(cursor.getColumnIndex("nazwisko")));
            kierowca.setLogin(cursor.getString(cursor.getColumnIndex("login")));
            kierowca.setHaslo(cursor.getString(cursor.getColumnIndex("haslo")));
            arrayOfKierowca.add(kierowca);

        }


        return arrayOfKierowca;
    }

    public Kierowca searchKierowca(Kierowca kierowcaBaza) {

        SQLiteDatabase db = getWritableDatabase();

        Kierowca kierowca = null;

        List<Kierowca> listOfKierowca = getListOfKierowca();
        Iterator<Kierowca> iterator = listOfKierowca.iterator();
        while(iterator.hasNext()) {
            kierowca = iterator.next();

            if(kierowcaBaza.getImie() != null && kierowcaBaza.getNazwisko()!=null) {
                if (kierowca.getHaslo().equals(kierowcaBaza.getHaslo().toString()) && kierowca.getLogin().equals(kierowcaBaza.getLogin().toString())
                        && kierowca.getImie().equals(kierowcaBaza.getImie().toString()) &&
                        kierowca.getNazwisko().equals(kierowcaBaza.getNazwisko().toString()))
                    return kierowca;
            }
            else {
                if (kierowca.getHaslo().toString().equals(kierowcaBaza.getHaslo().toString())
                        && kierowca.getLogin().toString().equals(kierowcaBaza.getLogin().toString()))
                    return kierowca;
            }

        }


        return null;
    }


    public Kierowca checkIfSimilar(Kierowca kierowcaBaza) {

        SQLiteDatabase db = getWritableDatabase();

        Kierowca kierowca = null;

        List<Kierowca> listOfKierowca = getListOfKierowca();
        Iterator<Kierowca> iterator = listOfKierowca.iterator();
        while(iterator.hasNext()) {
            kierowca = iterator.next();
            if (kierowca.getHaslo().equals(kierowcaBaza.getHaslo().toString()) && kierowca.getLogin().equals(kierowcaBaza.getLogin().toString()))
                return kierowca;
        }


        return null;

    }


    public boolean modyfyKierowcaData(Kierowca kierowca) {

        SQLiteDatabase db = getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(HASLO_KIEROWCA,kierowca.getHaslo().toString());
        contentValues.put(LOGIN_KIEROWCA,kierowca.getLogin().toString());
        contentValues.put(IMIE_KIEROWCA,kierowca.getImie().toString());
        contentValues.put(NAZWISKO_KIEROWCA,kierowca.getNazwisko().toString());
        

        long czyUdalosie = db.update(NAZWA_BAZY_KIEROWCA,contentValues,ID_KIEROWCA + "=?",new String[] {String.valueOf(kierowca.getId())});

        if(czyUdalosie == 0) return false;
        else return true;
    }


}
