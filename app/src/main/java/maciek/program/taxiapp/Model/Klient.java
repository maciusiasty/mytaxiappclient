package maciek.program.taxiapp.Model;

/**
 * Created by root on 23.01.17.
 */

public class Klient {

    private int id;
    private String imie;
    private String nazwisko;
    private String haslo;
    private String login;


    public Klient() {}

    public Klient(int id, String imie,String nazwisko,String haslo,String login) {
        this.id = id;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.haslo = haslo;
        this.login = login;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHaslo() {
        return haslo;
    }

    public void setHaslo(String haslo) {
        this.haslo = haslo;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }
}
