package maciek.program.taxiapp.Model;

/**
 * Created by maciek on 03.03.17.
 */


public class Stawka {

    private double stawka;

    public double getStawka() {
        return stawka;
    }

    public void setStawka(double stawka) {
        this.stawka = stawka;
    }
}