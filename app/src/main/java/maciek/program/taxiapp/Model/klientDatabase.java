package maciek.program.taxiapp.Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by root on 01.02.17.
 */

public class klientDatabase extends SQLiteOpenHelper {

    /*
            TABELA KLIENT
 ________________________________________________________
                id INTEGER PRIMARY KEY
                imieINazwisko TEXT NOT NULL
                ktoryRaz INTEGER NOT NULL
                ocena TEXT INTEGER NOT NULL
 ________________________________________________________
*/
    private static final String NAZWA_BAZY_KLIENT = "klient2";
    private static final String ID_KLIENT = "id";
    private static final String IMIE_KLIENT = "imie";
    private static final String NAZWISKO_KLIENT = "nazwisko";
    private static final String LOGIN_KLIENT="login";
    private static final String HASLO_KLIENT = "haslo";
    private static final int WERSJA_BAZY_KLIENT = 1;


    public static  klientDatabase instance = null;

    public klientDatabase(Context context) {
        super(context,NAZWA_BAZY_KLIENT, null, WERSJA_BAZY_KLIENT);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String klientSQL = "CREATE TABLE "+NAZWA_BAZY_KLIENT+"("+ID_KLIENT+" INTEGER PRIMARY KEY,"+
                IMIE_KLIENT + " TEXT NOT NULL,"+
                NAZWISKO_KLIENT+" TEXT NOT NULL,"+
                LOGIN_KLIENT + " TEXT NOT NULL,"+
                HASLO_KLIENT + " TEXT NOT NULL);";

        db.execSQL(klientSQL);

    }




    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String update = "DROP TABLE IF EXISTS "+NAZWA_BAZY_KLIENT;
        db.execSQL(update);
        onCreate(db);
    }


    public boolean addKlient(Klient klient) {

        int czyUdalosie = -1;

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        sqLiteDatabase.beginTransaction();

        Klient klientSzukaj = checkIfSimilar(klient);
        if(klientSzukaj !=null)
            return false;


        try {
            ContentValues contentValues = new ContentValues();

            contentValues.put(IMIE_KLIENT, klient.getImie());
            contentValues.put(NAZWISKO_KLIENT,klient.getNazwisko());
            contentValues.put(HASLO_KLIENT,klient.getHaslo());
            contentValues.put(LOGIN_KLIENT,klient.getLogin());

            czyUdalosie = (int) sqLiteDatabase.insertOrThrow(NAZWA_BAZY_KLIENT, null, contentValues);
            sqLiteDatabase.setTransactionSuccessful();
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        finally {
            sqLiteDatabase.endTransaction();

            if(czyUdalosie > -1) return true;
            else return false;
        }

    }



    public List<Klient> getListOfKlient() {

        String MY_KIEROWCA_QUERY = String.format("SELECT * FROM %s ",
                NAZWA_BAZY_KLIENT);

        SQLiteDatabase db = getWritableDatabase();

        Cursor cursor = db.rawQuery(MY_KIEROWCA_QUERY,null);

        ArrayList<Klient> arrayOfKlient = new ArrayList<Klient>();

        while(cursor.moveToNext()) {

           Klient klient = new Klient();
            klient.setHaslo(cursor.getString(cursor.getColumnIndex("haslo")));
            klient.setImie(cursor.getString(cursor.getColumnIndex("imie")));
            klient.setNazwisko(cursor.getString(cursor.getColumnIndex("nazwisko")));
            klient.setLogin(cursor.getString(cursor.getColumnIndex("login")));
            arrayOfKlient.add(klient);
        }
        return arrayOfKlient;
    }


   public Klient searchKlient(Klient klientBaza) {
        SQLiteDatabase db = getWritableDatabase();

        Klient klient = null;

        List<Klient> listKlient = getListOfKlient();
        Iterator<Klient> iterator = listKlient.iterator();
        while(iterator.hasNext()) {
           klient = iterator.next();
            if(klientBaza.getImie() != null && klientBaza.getNazwisko()!=null)  {
                if (klient.getHaslo().equals(klientBaza.getHaslo().toString()) && klient.getLogin().equals(klientBaza.getLogin().toString())
                        && klient.getImie().equals(klientBaza.getImie().toString())
                        && klient.getNazwisko().equals(klient.getNazwisko().toString()))
                    return klient;
            }
            else {
                if (klient.getHaslo().toString().equals(klientBaza.getHaslo().toString())
                        && klient.getLogin().toString().equals(klientBaza.getLogin().toString()))
                    return klient;
            }
        }


        return null;
    }


    public Klient checkIfSimilar(Klient klientBaza) {

        SQLiteDatabase db = getWritableDatabase();

        Klient klient = null;

        List<Klient> listOfKlient = getListOfKlient();
        Iterator<Klient> iterator = listOfKlient.iterator();
        while(iterator.hasNext()) {
            klient = iterator.next();
            if (klient.getHaslo().equals(klientBaza.getHaslo().toString()) && klient.getLogin().equals(klientBaza.getLogin().toString()))
                return klient;
        }


        return null;

    }


    public boolean modyfyKlientData(Klient klient) {

        SQLiteDatabase db = getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(HASLO_KLIENT,klient.getHaslo().toString());
        contentValues.put(LOGIN_KLIENT,klient.getLogin().toString());
        contentValues.put(IMIE_KLIENT,klient.getImie().toString());
        contentValues.put(NAZWISKO_KLIENT,klient.getNazwisko().toString());

       long czyUdalosie = db.update(NAZWA_BAZY_KLIENT,contentValues,ID_KLIENT + "=?",new String[] {String.valueOf(klient.getId())});

        if(czyUdalosie == 0) return false;
        else return true;
    }



}
