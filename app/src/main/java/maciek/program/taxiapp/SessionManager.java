package maciek.program.taxiapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.HashMap;

import maciek.program.taxiapp.Acitivity.LoginActivity;
import maciek.program.taxiapp.Acitivity.MainActivity;
import maciek.program.taxiapp.Acitivity.glownyActivity;

/**
 * Created by root on 02.02.17.
 */

public class SessionManager {

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "AndroidHivePref";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    //Czy Klient czy Kierowca
    public static final String IS_CLIENT = "IsClient";

    // User name (make variable public to access from outside)
    public static final String KEY_LOGIN = "login";

    // Email address (make variable public to access from outside)
    public static final String KEY_PASSWORD = "haslo";

    public static SessionManager instance = null;

    private SessionManager(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        editor = pref.edit();
    }


    public static SessionManager getInstance(Context context) {
        if(instance == null) {
            instance = new SessionManager(context);
            return instance;
        } else {
            return instance;
        }
    }

    public void createLoginSession(String login,String haslo,String konto,boolean isClient) {


        editor.putBoolean(IS_LOGIN,true);
        editor.putString(KEY_LOGIN,login);
        editor.putString(KEY_PASSWORD,haslo);
        if(isClient)
        editor.putString(IS_CLIENT,"klient");
        else editor.putString(IS_CLIENT,"kierowca");

        editor.commit();

        Intent intentGlowny = new Intent(context,glownyActivity.class);
        intentGlowny.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intentGlowny.putExtra("uzytkownik",konto);

        // Add new Flag to start new Activity
        intentGlowny.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(intentGlowny);

    }

    public HashMap<String , String> getUserDetails() {
        HashMap<String,String> hashMap = new HashMap<String,String>();

        hashMap.put(KEY_LOGIN,pref.getString(KEY_LOGIN,null));
        hashMap.put(KEY_PASSWORD,pref.getString(KEY_PASSWORD,null));
        hashMap.put(IS_CLIENT,pref.getString(IS_CLIENT,null));
        return hashMap;
    }

    public void checkLogin() {
        if(!isLogin()) {
            Intent intentLogin = new Intent(context,LoginActivity.class);
            intentLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intentLogin);
        }
    }

    public boolean isLogin() {
        return pref.getBoolean(IS_LOGIN,false);
    }

    public void logout() {

        editor.putBoolean(IS_LOGIN,false);
        editor.putString(KEY_LOGIN,null);
        editor.putString(KEY_PASSWORD,null);


        Intent powrotIntent = new Intent(context,LoginActivity.class);
        powrotIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        powrotIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(powrotIntent);
    }



}
