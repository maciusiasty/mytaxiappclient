package maciek.program.taxiapp;

/**
 * Created by maciek on 24.03.17.
 */

public interface TCPListener {

        public void onTCPMessageRecieved(String message);
        public void onTCPConnectionStatusChanged(boolean isConnectedNow);

}
