package maciek.program.taxiapp;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;

import maciek.program.taxiapp.Model.DataListView;

/**
 * Created by root on 13.02.17.
 */

public class OpcjeAdapter extends BaseAdapter {

    private ArrayList<DataListView> data;
    private Context context;
    private LayoutInflater layoutInflater;
    private Drawable[] image;

    private static class ViewHolder {
        TextView tytul;
        TextView opis;
        ImageView image;
    }

    public OpcjeAdapter(Context context, ArrayList<DataListView> data,Drawable[] imageData) {
        this.context = context;
        this.data = data;
        image = imageData;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        DataListView dataListView = data.get(position);

        // Check if an existing view is being reused, otherwise inflate the view

        if (convertView == null) {

            convertView = LayoutInflater.from(context).inflate(R.layout.simple_row_opcje, parent, false);

        }

        // Lookup view for data population

        ViewHolder viewHolder = new ViewHolder();
        viewHolder.tytul = (TextView) convertView.findViewById(R.id.tytul_opcje);
        viewHolder.opis = (TextView) convertView.findViewById(R.id.opis_opcje);
        viewHolder.image = (ImageView) convertView.findViewById(R.id.image_opcje);

        // Populate the data into the template view using the data object

        viewHolder.tytul.setText(dataListView.getTytul());
        viewHolder.image.setImageDrawable(image[position]);
        viewHolder.opis.setText(dataListView.getOpis());

        // Return the completed view to render on screen

        return convertView;
    }

}
