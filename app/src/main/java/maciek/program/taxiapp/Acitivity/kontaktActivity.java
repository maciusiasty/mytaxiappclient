package maciek.program.taxiapp.Acitivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.HashMap;

import maciek.program.taxiapp.R;
import maciek.program.taxiapp.SessionManager;

/**
 * Created by root on 28.01.17.
 */

public class kontaktActivity extends Activity implements View.OnClickListener {

    private Button powrot;
    private Context context;
    private boolean czyKlient;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_kontakt);

        context = getApplicationContext();

        sessionManager = SessionManager.getInstance(context);

        initButton();

        initData();
    }

    private void initButton() {
        powrot = (Button) findViewById(R.id.powrotKontakt);
        powrot.setOnClickListener(this);
    }

    private void initData() {
        HashMap<String,String> userDetails = sessionManager.getUserDetails();
        if(userDetails.get(SessionManager.IS_CLIENT).equals("klient")) czyKlient = true; else czyKlient = false;

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.powrotKontakt:
                Intent intentPowrot = new Intent(context,glownyActivity.class);
                startActivity(intentPowrot);
                finish();
        }
    }
}
