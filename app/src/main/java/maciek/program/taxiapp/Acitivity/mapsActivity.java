package maciek.program.taxiapp.Acitivity;

import android.Manifest;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.util.Log;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import maciek.program.taxiapp.DirectionsJSONParser;
import maciek.program.taxiapp.EnumsAndStatics;
import maciek.program.taxiapp.Fragment.dialogMiejsceFragment;
import maciek.program.taxiapp.GPSTracker;
import maciek.program.taxiapp.Model.Stawka;
import maciek.program.taxiapp.Model.StawkaFile;
import maciek.program.taxiapp.MyToolbarInterface;
import maciek.program.taxiapp.R;
import maciek.program.taxiapp.SessionManager;
import maciek.program.taxiapp.TCPCommunicator;
import maciek.program.taxiapp.TCPListener;

/**
 * Created by root on 12.02.17.
 */

public final class mapsActivity extends FragmentActivity implements OnMapReadyCallback,MyToolbarInterface,GoogleMap.OnMapClickListener
,NavigationView.OnNavigationItemSelectedListener,TCPListener {

    private static GoogleMap mgoogleMap;
    private boolean czyKlient;
    private static GPSTracker gpsTracker;
    private static Context context;
    private static SupportMapFragment supportMapFragment;
    private static Toolbar toolbar;
    private static  ArrayList<LatLng> markerPoints;
    private static TextView tvDistanceDuration;
    private static DrawerLayout drawer;
    private static dialogMiejsceFragment dFragment;
    private  FragmentManager fm = getSupportFragmentManager();
    private static FloatingActionButton fab;
    public static mapsActivity mActivity;
    private SessionManager sessionManager;
    private TCPCommunicator tcpCommunicator;
    private ProgressDialog progressDialog;
    private boolean isFirstLoad = true;
    private TCPCommunicator writer;
    private ListView listViewMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initializing
        markerPoints = new ArrayList<LatLng>();
        setContentView(R.layout.activity_map_nav);
        context = getApplicationContext();


        toolbar = (Toolbar) findViewById(R.id.toolbar_map);
        setActionBar(toolbar);

        sessionManager = SessionManager.getInstance(context);
        initTCPCommunitaction();

        initData();

        try {
            initializeMap();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

            gpsTracker = new GPSTracker(mapsActivity.this);
            gpsTracker.showAlertDialog();


        setupDrawer();

        initButton();

    }


    /*
    @Description inicjalizuje połączenie TCP (klient - serwer)
     */

    private void initTCPCommunitaction() {
        tcpCommunicator = TCPCommunicator.getInstance();
        TCPCommunicator.addListener(this);
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        tcpCommunicator.init(settings.getString(EnumsAndStatics.SERVER_IP_PREF, "192.168.1.100"),
                Integer.parseInt(settings.getString(EnumsAndStatics.SERVER_PORT_PREF, "1500")));
    }

    /*
    @Description Pokazuje pasek postępu
     */

    private void showProgressDialog() {
        progressDialog = new ProgressDialog(context,ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Ładowanie...");
        progressDialog.setMessage("Trwa ładowanie...");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
    }

    public static mapsActivity getInstanceOfClass() {
        if(mActivity == null) {
            mActivity = new mapsActivity();
            return mActivity;
        } else return mActivity;
    }


    /*
    @Description Laduje wzorzec typu Navigation drawer
     */
    private void setupDrawer() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer,null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
         drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

        mgoogleMap = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return true;
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

     switch(item.getItemId()) {
         case R.id.nav_search:
             dFragment = new dialogMiejsceFragment();
             dFragment.setContext(context);
             dFragment.setTcpCommunicator(tcpCommunicator);
             dFragment.show(fm,"Dialog Fragment");
             break;
         default:
             Toast.makeText(context,"Bład - opcja z poza zakresu",Toast.LENGTH_LONG).show();
     }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

  /*
  @Description inicjuje widgety dla maps Activity
   */

    private void initButton() {

        tvDistanceDuration = (TextView) findViewById(R.id.duration_map);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                markerPoints.clear();
                mgoogleMap.clear();
                onMapReady(mgoogleMap);
            }
        });

    }

/*
@Description obluga danych logowania
*/

    private void initData() {

        HashMap<String,String> userDetails = sessionManager.getUserDetails();
        if(userDetails.get(SessionManager.IS_CLIENT).equals("klient"))
        czyKlient = true; else
            czyKlient = false;
    }

    /*
    @Description Ustawia mape(inicjalizuje)
     */
    private void initializeMap() {
        if (mgoogleMap == null) {
           supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
           supportMapFragment.getMapAsync(this);
        }
        else {
            onMapReady(mgoogleMap);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        if(!isFirstLoad)
        {
            TCPCommunicator.closeStreams();
            initTCPCommunitaction();
        }
        else
            isFirstLoad=false;


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.mgoogleMap = googleMap;

        mgoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mgoogleMap.setMyLocationEnabled(true);
        mgoogleMap.setTrafficEnabled(true);
        mgoogleMap.setIndoorEnabled(true);
        mgoogleMap.setBuildingsEnabled(true);
        mgoogleMap.getUiSettings().setZoomControlsEnabled(true);

        mgoogleMap.setOnMapClickListener(this);
        if(gpsTracker.checkIfCanGetLocation()) {

            LatLng latLng = new LatLng(gpsTracker.getLatitude(),gpsTracker.getLongitude());
            markerPoints.add(latLng);
            mgoogleMap.addMarker(new MarkerOptions().position(latLng).title("Aktualna pozycja").
                    icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
            tvDistanceDuration.setText("");
            mgoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));


        }
        else Toast.makeText(context,"Bląd przy znalezieniu lokalizacji.",Toast.LENGTH_LONG).show();


    }

    public void dodajMiejsceZNavigationMenu(LatLng latLng) {
       onMapClick(latLng);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case R.id.normal_map:
                mgoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
            case R.id.hybrid_map:
                mgoogleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                break;
            case R.id.satellite_map:
                mgoogleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.terrain_map:
                mgoogleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                break;
            case R.id.none_map:
                mgoogleMap.setMapType(GoogleMap.MAP_TYPE_NONE);
                break;
        }
        return true;
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest){

        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;

        return url;
    }

    /** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException{
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Log.d("Blad przy :",e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    @Override
    public void onMapClick(LatLng point) {
        //Jesli zadanie jest wyzwalane z zewnatrz

        if (markerPoints.size() > 1) {
            markerPoints.clear();
            mgoogleMap.clear();
            tvDistanceDuration.setText("");
        }


        // Adding new item to the ArrayList
        markerPoints.add(point);

        // Creating MarkerOptions
        MarkerOptions options = new MarkerOptions();

        // Setting the position of the marker
        options.position(point);

        /**
         * For the start location, the color of marker is GREEN and
         * for the end location, the color of marker is RED.
         */
        if (markerPoints.size() == 1) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        } else if (markerPoints.size() == 2) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        }

        // Add new marker to the Google Map Android API V2
        mgoogleMap.addMarker(options);

        // Checks, whether start and end locations are captured
        if (markerPoints.size() >= 2) {
            LatLng origin = markerPoints.get(0);
            LatLng dest = markerPoints.get(1);

            // Getting URL to the Google Directions API
            String url = getDirectionsUrl(origin, dest);

            DownloadTask downloadTask = new DownloadTask();

            // Start downloading json data from Google Directions API
            downloadTask.execute(url);
        }
    }

    @Override
    public void onTCPMessageRecieved(String message) {

        String mMessage = message;


        try {
            JSONObject obj = new JSONObject(message);
            String messageTypeString = obj.getString(EnumsAndStatics.MESSAGE_TYPE_FOR_JSON);
            EnumsAndStatics.MessageTypes messageType = EnumsAndStatics.getMessageTypeByString(messageTypeString);

            switch(messageType) {
                case MessageFromServer:

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Toast.makeText(context,"Wiadomość od serwera.",Toast.LENGTH_LONG).show();
                        }
                    });
                    break;
            }
        } catch(JSONException ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void onTCPConnectionStatusChanged(boolean isConnectedNow) {

        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                progressDialog.hide();
                Toast.makeText(getApplicationContext(), "Connected to server", Toast.LENGTH_SHORT).show();
            }
        });

    }


    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String>{

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> > {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            String distance = "";
            String duration = "";

            if(result.size()<1){
                Toast.makeText(getBaseContext(), "No Points", Toast.LENGTH_SHORT).show();
                return;
            }

            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    if(j==0){    // Get distance from the list
                        distance = (String)point.get("distance");
                        continue;
                    }else if(j==1){ // Get duration from the list
                        duration = (String)point.get("duration");
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(8);
                lineOptions.color(Color.RED);
            }


            StawkaFile stawkaFile = new StawkaFile();
            Stawka stawka = stawkaFile.czytajStawka(context);
            double doZaplacenia = stawka.getStawka();
            if(stawka == null) Toast.makeText(context,"Stawka nie została ustawione (menu opcje).",Toast.LENGTH_LONG).show();

            tvDistanceDuration.setText("Dystans:"+distance + ", Czas:"+duration+ " \n Do zapłacenia:"+
                    Math.ceil(Double.valueOf(doZaplacenia*Double.valueOf(distance.replace("km",""))))+" zł");

            // Drawing polyline in the Google Map for the i-th route
            mgoogleMap.addPolyline(lineOptions);
        }
    }



}
