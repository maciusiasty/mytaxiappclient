package maciek.program.taxiapp.Acitivity;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.widget.TextView;

import maciek.program.taxiapp.R;

/**
 * Created by maciek on 24.03.17.
 */

public class LocateActivity extends FragmentActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private TextView lokalizacja_nazwa;
    private Uri mUri;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.item_searchable_activity);


        initWidget();
        getSupportLoaderManager().initLoader(0,null,this);
    }

    private void initWidget() {
        lokalizacja_nazwa = (TextView) findViewById(R.id.nameOfLocation);

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getBaseContext(), mUri, null, null , null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        lokalizacja_nazwa.setText(data.getString(data.getColumnIndex(data.getColumnName(0))));
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
