package maciek.program.taxiapp.Acitivity;


import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

import maciek.program.taxiapp.AlertDialogManager;
import maciek.program.taxiapp.Model.Kierowca;
import maciek.program.taxiapp.Model.Klient;
import maciek.program.taxiapp.R;
import maciek.program.taxiapp.SessionManager;
import maciek.program.taxiapp.Model.kierowcaDatabase;
import maciek.program.taxiapp.Model.klientDatabase;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button button_zaloguj;
    private EditText login,haslo;
    private Context context;
    private AlertDialogManager alertDialogManager;
    private SessionManager sessionManager;
    private klientDatabase klDatabase;
    private TextView link_signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        context = getApplicationContext();

        klDatabase = new klientDatabase(context);


        sessionManager = SessionManager.getInstance(context);
        alertDialogManager = new AlertDialogManager();
        initWidget();

    }


    private void initWidget() {
        context = getApplicationContext();
        button_zaloguj = (Button) findViewById(R.id.zaloguj);
        button_zaloguj.setOnClickListener(this);
        login = (EditText) findViewById(R.id.login);
        haslo = (EditText) findViewById(R.id.haslo);
        link_signup = (TextView) findViewById(R.id.link_signup);

        link_signup.setOnClickListener(this);;


    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {


            case R.id.zaloguj:
                Klient klient = null;

                    Klient klientBaza = new Klient();
                    klientBaza.setLogin(login.getText().toString());
                    klientBaza.setHaslo(haslo.getText().toString());
                    klient = klDatabase.searchKlient(klientBaza);

                if(klient !=null) {
                    sessionManager.createLoginSession(klient.getLogin().toString(), klient.getHaslo().toString(),"klient",true);
                    alertDialogManager.showAlertDialog(LoginActivity.this,"Logowanie","Zalogowano się jako "+klient.getLogin().toString(),true);
                }
                else
                alertDialogManager.showAlertDialog(LoginActivity.this,"Logowanie","Zły login lub haslo",false);
                break;

            case R.id.link_signup:
                Intent intentSignUp = new Intent(this,rejestracjaActivity.class);
                startActivity(intentSignUp);
                break;

            default:
                Toast.makeText(context,"Blad programu",Toast.LENGTH_SHORT).show();
        }
    }
}
