package maciek.program.taxiapp.Acitivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import maciek.program.taxiapp.Model.Kierowca;
import maciek.program.taxiapp.Model.Klient;
import maciek.program.taxiapp.R;
import maciek.program.taxiapp.Model.kierowcaDatabase;
import maciek.program.taxiapp.Model.klientDatabase;

/**
 * Created by root on 01.02.17.
 */

public class rejestracjaActivity extends Activity implements View.OnClickListener {

    private Button rejestruj_button,powrot_button;
    private Context context;
    private String str;
    private kierowcaDatabase kDatabase;
    private klientDatabase klDatabase;
    private EditText login,haslo,imie,nazwisko;


    private TextView link_login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        context = getApplicationContext();

        klDatabase = new klientDatabase(context);

        initButton();
    }

    private void initButton() {

        rejestruj_button = (Button) findViewById(R.id.rejestruj);

        link_login = (TextView)findViewById(R.id.link_login);
        link_login.setOnClickListener(this);
        powrot_button = (Button) findViewById(R.id.powrot);
        powrot_button.setOnClickListener(this);
        rejestruj_button.setOnClickListener(this);
        login = (EditText) findViewById(R.id.login);
        haslo = (EditText) findViewById(R.id.haslo);
        imie = (EditText) findViewById(R.id.imie);
        nazwisko = (EditText) findViewById(R.id.nazwisko);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rejestruj:
                boolean czyUdalosie;

                     Klient klient = new Klient();
                     klient.setImie(imie.getText().toString());
                    klient.setNazwisko(nazwisko.getText().toString());
                     klient.setLogin(login.getText().toString());
                     klient.setHaslo(haslo.getText().toString());
                     czyUdalosie =   klDatabase.addKlient(klient);
                if(czyUdalosie) Toast.makeText(context,"Dane zostaly dodane do bazy danych",Toast.LENGTH_LONG).show();
                else Toast.makeText(context,"Błąd przy dodwania danych do bazy danych, zdefiniowane dane mogą już istnieć",Toast.LENGTH_LONG).show();

                break;
            case R.id.powrot:
                Intent powrt =new Intent(context,LoginActivity.class);
                startActivity(powrt);
                finish();
                break;

            case R.id.link_login:
                Intent loginPowrot = new Intent(context,MainActivity.class);
                startActivity(loginPowrot);
                finish();
                break;
        }

    }
}
