package maciek.program.taxiapp.Acitivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;

import maciek.program.taxiapp.Model.Kierowca;
import maciek.program.taxiapp.Model.Klient;
import maciek.program.taxiapp.Model.kierowcaDatabase;
import maciek.program.taxiapp.Model.klientDatabase;
import maciek.program.taxiapp.R;
import maciek.program.taxiapp.SessionManager;

/**
 * Created by root on 13.02.17.
 */

public class accountActivity extends Activity implements View.OnClickListener {

    private EditText imieZmien,nazwiskoZmien,loginZmien,hasloZmien;
    private Button zmodyfikujDane,powrotZmien;
    private boolean czyKlient;
    private kierowcaDatabase kierDatabase;
    private klientDatabase klDatabase;
    private Context context;

    private Klient klient;
    private Kierowca kierowca;
    private SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = getApplicationContext();

        setContentView(R.layout.activity_account);

        kierDatabase = new kierowcaDatabase(context);
        klDatabase = new klientDatabase(context);
        sessionManager = SessionManager.getInstance(context);

        initButton();

        ustawDane();


    }


    private void initButton() {

        imieZmien = (EditText) findViewById(R.id.imieZmien);
        nazwiskoZmien = (EditText) findViewById(R.id.nazwiskoZmien);
        loginZmien = (EditText) findViewById(R.id.loginZmien);
        hasloZmien = (EditText) findViewById(R.id.hasloZmien);
        powrotZmien = (Button) findViewById(R.id.powrotZmien);
        zmodyfikujDane = (Button) findViewById(R.id.ZmienDane);

        powrotZmien.setOnClickListener(this);
        zmodyfikujDane.setOnClickListener(this);

        HashMap<String,String> userDetails = sessionManager.getUserDetails();
        if(userDetails.get(SessionManager.IS_CLIENT).equals("klient"))
            czyKlient = true;
        else czyKlient = false;
    }


    private void ustawDane() {

        SessionManager sessionManager = SessionManager.getInstance(context);
         HashMap<String,String> dataUsers = sessionManager.getUserDetails();
        if(!dataUsers.isEmpty()) {
            String login = dataUsers.get(sessionManager.KEY_LOGIN);
            String haslo = dataUsers.get(sessionManager.KEY_PASSWORD);
            if(czyKlient) {

                Klient klientBaza = new Klient();
                klientBaza.setHaslo(haslo);
                klientBaza.setLogin(login);
                klient = klDatabase.searchKlient(klientBaza);
                imieZmien.setText(klient.getImie().toString());
                nazwiskoZmien.setText(klient.getNazwisko().toString());
                loginZmien.setText(klient.getLogin().toString());
                hasloZmien.setText(klient.getHaslo().toString());

            } else {

                Kierowca kierowcaBaza = new Kierowca();
                kierowcaBaza.setLogin(login);
                kierowcaBaza.setHaslo(haslo);
                kierowca = kierDatabase.searchKierowca(kierowcaBaza);
                imieZmien.setText(kierowca.getImie().toString());
                nazwiskoZmien.setText(kierowca.getNazwisko().toString());
                loginZmien.setText(kierowca.getLogin().toString());
                hasloZmien.setText(kierowca.getHaslo().toString());

            }
        }


    }



   private void zmienDane() {

       String imie = imieZmien.getText().toString();
       String nazwisko = nazwiskoZmien.getText().toString();
       String login = loginZmien.getText().toString();
       String haslo = hasloZmien.getText().toString();

       if(czyKlient) {
          klient.setNazwisko(nazwisko);
           klient.setHaslo(haslo);
           klient.setImie(imie);
           klient.setLogin(login);
           /*
           TODO BLAD przy modyfikacji danych
            */
           boolean czyUdalosie = klDatabase.modyfyKlientData(klient);
           if(czyUdalosie) Toast.makeText(context,"Dane zostaly zmienione",Toast.LENGTH_LONG).show();
           else Toast.makeText(context,"Blad przy zmianie danych",Toast.LENGTH_LONG).show();
       }
       else  {
           kierowca.setLogin(login);
           kierowca.setImie(imie);
           kierowca.setNazwisko(nazwisko);
           kierowca.setHaslo(haslo);
           boolean czyUdalosie = kierDatabase.modyfyKierowcaData(kierowca);
           if(czyUdalosie) Toast.makeText(context,"Dane zostały zmienione",Toast.LENGTH_LONG).show();
           else Toast.makeText(context,"Błąd przy zmienie danych",Toast.LENGTH_LONG).show();
       }


   }



    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.ZmienDane:
            zmienDane();
                break;
            case R.id.powrotZmien:
                Intent powrotZmien = new Intent(this,glownyActivity.class);
                startActivity(powrotZmien);
                finish();
                break;
        }
    }


}
