package maciek.program.taxiapp.Acitivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import maciek.program.taxiapp.Model.DataListView;
import maciek.program.taxiapp.Model.Stawka;
import maciek.program.taxiapp.Model.StawkaFile;
import maciek.program.taxiapp.OpcjeAdapter;
import maciek.program.taxiapp.R;
import maciek.program.taxiapp.SessionManager;

/**
 * Created by root on 13.02.17.
 */

public class opcjeActivity extends Activity  {

    private Context context;
    Drawable[] drawables;
    private OpcjeAdapter opcjeAdapter;
    ArrayList<DataListView> dataListViews;
    private ListView listView;
    private boolean czyKlient;
    private Button ustawStawkePLN;
    private EditText stawkaPLN;
    private Button powrotDoMenu,powrotDoMenuDialog;
    private SessionManager sessionManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_opcje);
        drawables = new Drawable[] { getResources().getDrawable(R.drawable.position_opcje),getResources().getDrawable(R.drawable.contact),getResources().getDrawable(R.drawable.payment)};
        generateDataListView();
        context = getApplicationContext();
        listView = (ListView) findViewById(R.id.listViewOpcje);
        opcjeAdapter = new OpcjeAdapter(context,dataListViews,drawables);
        listView.setAdapter(opcjeAdapter);

        sessionManager = SessionManager.getInstance(context);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch(position) {
                    case 0:
                        Toast.makeText(context,"Wybrano opcje nr."+position,Toast.LENGTH_LONG).show();
                        break;
                    case 1:
                        Intent kotnaktIntent = new Intent(context,kontaktActivity.class);
                        if(czyKlient)
                            kotnaktIntent.putExtra("uzytkownik","klient");
                        else kotnaktIntent.putExtra("uzytkownik","kierowca");
                        startActivity(kotnaktIntent);
                        finish();
                        break;
                    case 2:
                        showDialogStawka();
                        break;
                    default:
                        Toast.makeText(getBaseContext(),"Blad przy wybieraniu opcji",Toast.LENGTH_LONG).show();
                }

            }
        });

        initData();

        initButton();

    }

    /*
    @Description inicjuje
     */
    private void initButton() {
        powrotDoMenu = (Button) findViewById(R.id.opcjePowrot);
        powrotDoMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentPowrot = new Intent(context,glownyActivity.class);
                startActivity(intentPowrot);
                finish();
            }
        });

    }


    private void initData() {
        HashMap<String,String> userDetails = sessionManager.getUserDetails();
        if(userDetails.get(SessionManager.IS_CLIENT).equals("klient")) czyKlient = true; else czyKlient = false;

    }

    /*
    @Description generuje listę danych.
     */

    private void generateDataListView() {
        dataListViews = new ArrayList<DataListView>();
        dataListViews.add(new DataListView("Wskaż moją pozycje na mapie.","Dodawanie pozycji na mapie aktualnego miejsca przebywania."));;
        dataListViews.add(new DataListView("Kontakt do nas.","Pokazuje możliwości skontatkowania się z admnistracją."));
        dataListViews.add(new DataListView("Stawka pieniężna","Określa stawkę w PLN / km"));
    }

    /*
    @Description pokazuje okno dialogowe z wyborem stawki
     */

    private void showDialogStawka() {

        final StawkaFile stawkaFile = new StawkaFile();

        final Dialog dialog = new Dialog(opcjeActivity.this);

        dialog.setContentView(R.layout.dialog_stawka);
        dialog.setTitle("Ustaw stawke PLN");

        ustawStawkePLN = (Button) dialog.findViewById(R.id.ustawStawkePLN);
        stawkaPLN = (EditText) dialog.findViewById(R.id.stawkaPLN);
        powrotDoMenuDialog = (Button) dialog.findViewById(R.id.powrotDoOpcji);
        powrotDoMenuDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        ustawStawkePLN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            Stawka stawka = new Stawka();
                stawka.setStawka(Double.parseDouble(stawkaPLN.getText().toString()));
               boolean czyUdaloSie = stawkaFile.dodajStawka(stawka,context);
                if(czyUdaloSie) Toast.makeText(context,"Dane zostały zapisane",Toast.LENGTH_LONG).show();
                else Toast.makeText(context,"Blad przy zapisywaniu danych.",Toast.LENGTH_LONG).show();

            }

        });
        dialog.show();

    }



}
