package maciek.program.taxiapp.Acitivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import java.util.Timer;
import java.util.TimerTask;

import maciek.program.taxiapp.Model.kierowcaDatabase;
import maciek.program.taxiapp.Model.klientDatabase;
import maciek.program.taxiapp.R;

/**
 * Created by root on 11.02.17.
 */

public class MainActivity extends Activity  {

    private ProgressBar progressBar;
    private klientDatabase klDatabase;
    private kierowcaDatabase kierDatabase;
    private Context context;
    private int i=0;
    private Thread thread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        initButton();
        context = getApplicationContext();


    }

    private void initButton() {

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);


            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        while (i < 10) {
                            i++;
                            Thread.sleep(1000);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Intent loginActivity = new Intent(getApplicationContext(),LoginActivity.class);
                    loginActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(loginActivity);
                }
            }).start();







    }


}
