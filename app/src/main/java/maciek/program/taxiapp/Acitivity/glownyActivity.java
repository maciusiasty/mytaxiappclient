package maciek.program.taxiapp.Acitivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.HashMap;

import maciek.program.taxiapp.R;
import maciek.program.taxiapp.SessionManager;

/**
 * Created by root on 02.02.17.
 */

public class glownyActivity extends Activity implements View.OnClickListener {

    private Context context;

    private Button button_wyjscie,button_szukaj,button_opcje,button_konto;
    private SessionManager sessionManager;
    private boolean czyKlient;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = getApplicationContext();

        setContentView(R.layout.activity_glowny);

        sessionManager = SessionManager.getInstance(context);


        initButton();
    }

    private void initButton() {

        button_opcje = (Button) findViewById(R.id.button_opcje);
        button_szukaj = (Button) findViewById(R.id.button_szukaj);
        button_wyjscie = (Button) findViewById(R.id.button_wyjscie);
        button_konto = (Button) findViewById(R.id.button_account);

        button_wyjscie.setOnClickListener(this);
        button_szukaj.setOnClickListener(this);
        button_opcje.setOnClickListener(this);
        button_konto.setOnClickListener(this);


        HashMap<String,String> userDetails = sessionManager.getUserDetails();
        if(userDetails.get(SessionManager.IS_CLIENT).equals("klient")) czyKlient = true; else czyKlient = false;
    }


    @Override
    public void onClick(View v) {

        switch(v.getId()) {

            case R.id.button_szukaj:
                Intent mapsIntent = new Intent(this,mapsActivity.class);
                startActivity(mapsIntent);
                break;
            case R.id.button_account:
                Intent accountIntent = new Intent(this,accountActivity.class);
                startActivity(accountIntent);
                break;
            case R.id.button_opcje:
               Intent opcjeIntent = new Intent(this,opcjeActivity.class);
                startActivity(opcjeIntent);
                break;
            case R.id.button_wyjscie:
                sessionManager.logout();
                break;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

      sessionManager.logout();
    }
}
