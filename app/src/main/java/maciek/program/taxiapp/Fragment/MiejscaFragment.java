package maciek.program.taxiapp.Fragment;

import android.app.Fragment;
import android.content.Context;
import android.location.Address;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import maciek.program.taxiapp.Acitivity.mapsActivity;
import maciek.program.taxiapp.R;

import static android.R.id.list;


/**
 * Created by maciek on 06.03.17.
 */

public class MiejscaFragment extends ListFragment {


    private Button ok;
    private List<Address> listOfAddress;
    private List<String> listOfData = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       return inflater.inflate(R.layout.fragmentmap,container,false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        transformToData(listOfAddress);
        ArrayAdapter<String> mAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,listOfData);

        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                mapsActivity mActivity = mapsActivity.getInstanceOfClass();
                LatLng latLng = new LatLng(listOfAddress.get(i).getLatitude(),listOfAddress.get(i).getLongitude());
                mActivity.dodajMiejsceZNavigationMenu(latLng);
            }
        });
        getListView().setAdapter(mAdapter);
    }


    public List<Address> getListOfAddress() {
        return listOfAddress;
    }


    public void setListOfAddress(List<Address> listOfAddress) {
        this.listOfAddress = listOfAddress;
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
       switch(position) {
           case 0:
               Toast.makeText(getActivity(),"Wybrano pozycje nr. "+position,Toast.LENGTH_LONG).show();
               break;
           default:
               Toast.makeText(getActivity(),"Dane z poza zakresu ",Toast.LENGTH_LONG).show();
       }
    }


    private void transformToData(List<Address> listOfAddress) {

        for(int i=0;i<listOfAddress.size();i++) {
            listOfData.add(listOfAddress.get(i).getCountryName() + ", "+listOfAddress.get(i).getLocality()+ " "+listOfAddress.get(i).getAdminArea());
        }
    }


}




