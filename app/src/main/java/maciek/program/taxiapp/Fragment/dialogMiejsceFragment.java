package maciek.program.taxiapp.Fragment;
;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import android.os.Handler;
import android.widget.ListView;
import android.widget.SearchView;

import maciek.program.taxiapp.EnumsAndStatics;
import maciek.program.taxiapp.GPSTracker;
import maciek.program.taxiapp.R;
import maciek.program.taxiapp.TCPCommunicator;

/**
 * Created by maciek on 13.03.17.
 */

public class dialogMiejsceFragment extends DialogFragment implements View.OnClickListener {

    private Button szukajDrogi,powrotDoMapy;
    private SearchView polorzenieDoceloweSzukaj;
    private String polorzenieDocelowe;
    private List<Address> listOfAddress;
    private GPSTracker gpsTracker;
    private View rootView;
    private TCPCommunicator tcpCommunicator;
    private Handler UIHandler = new Handler();
    private Context context;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_map,container,false);
        this.rootView = view;

         getDialog().setTitle("Ustawienie docelowego miejsca");
         gpsTracker = new GPSTracker(getActivity());
         initButton();



        return view;
    }


    public void setTcpCommunicator(TCPCommunicator tcpCommunicator) {
        this.tcpCommunicator = tcpCommunicator;
    }


    /*
    @Description inicjacja komponentów
    */
    private void initButton() {

        szukajDrogi = (Button) rootView.findViewById(R.id.szukajDrogi);
        powrotDoMapy = (Button) rootView.findViewById(R.id.powrotDoMapy);
        polorzenieDoceloweSzukaj = (SearchView) rootView.findViewById(R.id.polorzenie_dialogMap);
        SearchManager searchManager = (SearchManager) context.getSystemService(Context.SEARCH_SERVICE);
        SearchableInfo searchableInfo = searchManager.getSearchableInfo(getActivity().getComponentName());
        polorzenieDoceloweSzukaj.setSearchableInfo(searchableInfo);

        szukajDrogi.setOnClickListener(this);
        powrotDoMapy.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            //Tutaj glowna robota dla dialog'a
            case R.id.szukajDrogi:

            szukajDrogi(view);

                break;
            case R.id.powrotDoMapy:
                getDialog().dismiss();
                break;
        }
    }

    /*
    @Description Metoda wykonywana przy wywołaniu button'a szukajDroga
     */
    private void szukajDrogi(View view) {

        polorzenieDocelowe = polorzenieDoceloweSzukaj.getQuery().toString();
        if(!polorzenieDocelowe.isEmpty()) {
            try {
                listOfAddress = gpsTracker.geoLocate(view, polorzenieDocelowe);
                MiejscaFragment miejscaFragment = new MiejscaFragment();
                miejscaFragment.setListOfAddress(listOfAddress);
                dodajFragmentMiejscaMapa(miejscaFragment);
            } catch(IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if(!polorzenieDocelowe.isEmpty()) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(EnumsAndStatics.MESSAGE_CONTENT_FOR_JSON, EnumsAndStatics.MessageTypes.MessageFromClient);
                jsonObject.put(EnumsAndStatics.MESSAGE_CONTENT_FOR_JSON,polorzenieDocelowe.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            tcpCommunicator.writeToSocket(jsonObject,UIHandler,getContext());
        }
    }

/*
    @Description ustawia fragment dotyczacy miejsca
     */

    private void dodajFragmentMiejscaMapa(MiejscaFragment miejscaFragment) {

        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        ListFragment fragment = miejscaFragment;
        fragmentTransaction.replace(R.id.fragment_miejsca,fragment);
        fragmentTransaction.commit();

    }

    public void setContext(Context context) {
        this.context = context;
    }
}
